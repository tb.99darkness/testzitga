﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

public class HeroManager : MonoBehaviour
{
    [SerializeField]
    private Transform heroHolder;
    [SerializeField]
    private Heros heros;
    [SerializeField]
    private TextAsset heroCSV;
    [ShowInInspector]
    private Dictionary<string, Pool<Hero>> heroPoolsDict;
    private Dictionary<string, AttackBase> herosTypeDict = new Dictionary<string, AttackBase>();
    private void Awake()
    {
        DataConverter.InitDataHero(heroCSV);
        herosTypeDict = DataConverter.GetDataFromHeroCSV();
        heroPoolsDict = new Dictionary<string, Pool<Hero>>();
        Dictionary<string, Hero> herosDict = new Dictionary<string, Hero>();
        int heroCount = heros.allHeros.Count;
        for (int i = 0; i < heroCount; i++)
        {
            string heroName = heros.allHeros[i].GetType().Name;
            Hero hero = heros.allHeros[i];
            hero.AttackBase = herosTypeDict[heroName];
            herosDict.Add(heroName, hero);
        }
        foreach (KeyValuePair<string, AttackBase> item in herosTypeDict)
        {
            string heroName = item.Key;
            heroPoolsDict.Add(heroName, new Pool<Hero>(herosDict[heroName], 5, heroHolder));
        }

    }
    [Button]
    public void GetHero(string heroName, AttackBase attack = null)
    {
        if (string.IsNullOrEmpty(heroName))
        {
            Debug.Log("give me hero name dude!");
            return;
        }
        Hero hero = heroPoolsDict[heroName].GetObject();
        hero.transform.position = new Vector3(Random.Range(-5, 5), 5, Random.Range(-5, 5));
        if (attack == null)
        {
            return;
        }
        hero.SetDependency(attack);
    }
}
