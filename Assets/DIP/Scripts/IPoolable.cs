﻿public interface IPoolable
{
    void ResetValue();

    void Depool();
}
