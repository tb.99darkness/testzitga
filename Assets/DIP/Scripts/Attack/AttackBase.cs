﻿using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class AttackBase : IAttack
{
    public EffectBase effect;
    public int NumberOfTarget;
    [HideInInspector]
    public List<Transform> target;

    public virtual void Attack()
    {
        effect.ApplyEffect();
    }
}
