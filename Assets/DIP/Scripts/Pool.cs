﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
[System.Serializable]
public class Pool<T> where T : MonoBehaviour, IPoolable
{
    private T Prefab;
    private int PreInstantiateNumbers;
    private Transform Parent;
    private List<T> avaiableObjectList = new List<T>();
    private List<T> usedObjectList = new List<T>();
    /// <summary>
    /// khởi tạo pool T
    /// </summary>
    /// <param name="prefab">prefab có chứa Ipoolable</param>
    /// <param name="preInstantiateNumbers">số lương object khởi tạo trong pool</param>
    /// <param name="parent">tranform của object chứa pool </param>
    public Pool(T prefab, int preInstantiateNumbers, Transform parent)
    {
        this.Prefab = prefab;
        this.Parent = parent;
        this.PreInstantiateNumbers = preInstantiateNumbers;
        for (int i = 0; i < PreInstantiateNumbers; i++)
        {
            T newObject = GameObject.Instantiate(prefab, Parent);
            CloneFields(ref prefab, ref newObject);
            CloneProperties(ref prefab, ref newObject);
            newObject.gameObject.SetActive(false);
            avaiableObjectList.Add(newObject);
        }
    }
    /// <summary>
    /// get Object T from Pool
    /// </summary>
    /// <param name="newparent"></param>
    /// <returns></returns>
    public T GetObject(Transform newparent = null)
    {
        T returnObject;
        if (avaiableObjectList.Count > 0)
        {
            avaiableObjectList[0].transform.SetParent(newparent);
            returnObject = avaiableObjectList[0];
            usedObjectList.Add(avaiableObjectList[0]);
            avaiableObjectList.RemoveAt(0);
            returnObject.gameObject.SetActive(true);
            return returnObject;
        }
        returnObject = GameObject.Instantiate(Prefab);
        usedObjectList.Add(returnObject);
        return returnObject;
    }
    /// <summary>
    /// depool object 
    /// </summary>
    /// <param name="depoolObject"></param>
    public void DepoolObject(T depoolObject)
    {
        usedObjectList.Remove(depoolObject);
        depoolObject.transform.SetParent(Parent);
        depoolObject.ResetValue();
        depoolObject.gameObject.SetActive(false);
        avaiableObjectList.Add(depoolObject);
    }

    private void CloneFields(ref T objectbase, ref T objectClone)
    {
        FieldInfo[] fields = objectbase.GetType().GetFields();
        for (int j = 0; j < fields.Length; j++)
        {
            FieldInfo field = objectClone.GetType().GetField(fields[j].Name, BindingFlags.Public | BindingFlags.Instance);
            if (field != null)
            {
                field.SetValue(objectClone, field.GetValue(objectbase));
            }
        }
    }

    private void CloneProperties(ref T objectbase, ref T objectClone)
    {
        PropertyInfo[] properties = objectbase.GetType().GetProperties();
        for (int j = 0; j < properties.Length; j++)
        {
            PropertyInfo property = objectClone.GetType().GetProperty(properties[j].Name, BindingFlags.Public | BindingFlags.Instance);
            if (property != null && property.CanWrite)
            {
                property.SetValue(objectClone, property.GetValue(objectbase));
            }
        }
    }
}
