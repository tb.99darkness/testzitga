﻿public interface IEffect
{
    void ApplyEffect();
}
