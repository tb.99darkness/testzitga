﻿using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
public class Hero : MonoBehaviour, IHeroDependency, IPoolable
{
    [SerializeField]
    private TextMeshProUGUI info;
    [SerializeField]
    private AttackBase attackBase;
    public AttackBase AttackBase
    {
        get
        {
            return attackBase;
        }
        set
        {
            attackBase = value;
            attackType = attackBase.GetType().ToString();
            effectType = attackBase.effect.GetType().ToString();

        }
    }
    [ReadOnly]
    public string attackType;
    [ReadOnly]
    public string effectType;
    private void Start()
    {
        DisplayInfo();
    }
    [Button]
    public void Attack()
    {
        attackBase.Attack();
        attackBase.effect.ApplyEffect();
    }

    public void SetDependency(AttackBase attackEffect)
    {
        this.AttackBase = attackEffect;
    }

    public void ResetValue()
    {
        transform.position = Vector3.zero;
        transform.rotation = Quaternion.identity;
    }
    public void Depool()
    {
        // em đang phân vân k biết nên để object tự depool chính nó hay sẽ có một bên thứ 3
    }
    private void DisplayInfo()
    {
        info.text = $"{this.GetType().Name} \n {attackBase.GetType().Name} \n {attackBase.effect.GetType().Name}";
    }
}
