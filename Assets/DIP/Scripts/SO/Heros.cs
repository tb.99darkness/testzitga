﻿using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "List Heros")]
public class Heros : ScriptableObject
{
    public List<Hero> allHeros = new List<Hero>();
}
