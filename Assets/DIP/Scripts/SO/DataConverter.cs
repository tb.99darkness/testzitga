﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class DataConverter
{
    private static TextAsset heroData;
    public static void InitDataHero(TextAsset text)
    {
        heroData = text;
    }
    /// <summary>
    /// Get Data From Hero CSV
    /// </summary>
    /// <param name="data">CSV file</param>
    /// <returns> key:hero name , value : Attack Type </returns>
    public static Dictionary<string, AttackBase> GetDataFromHeroCSV(TextAsset data = null)
    {
        if (data == null)
        {
            data = heroData;
        }
        Dictionary<string, AttackBase> heroTypeDictTemp = new Dictionary<string, AttackBase>();
        string[] rows = data.text.Trim().Split('\n');
        for (int i = 1; i < rows.Length; i++)
        {
            string[] colums = rows[i].Split(',');
            string heroType = colums[(int)column.Hero];
            string effectType = colums[(int)column.Effect].Trim();
            string attackType = colums[(int)column.AttackType].Trim();
            AttackBase attack = FindIAttackByName(attackType);
            attack.effect = FindIEffectByName(effectType);
            attack.NumberOfTarget = Int32.Parse(colums[(int)column.NumberOfTarget].Trim());
            attack.effect.EffectDuration = float.Parse(colums[(int)column.EffectDuration].Trim());
            heroTypeDictTemp.Add(heroType, attack);
        }
        return heroTypeDictTemp;
    }
    public static List<string> GetAllHeroNameFromHeroCSV(TextAsset data = null)
    {
        if (data == null)
        {
            data = heroData;
        }
        List<string> heroNames = new List<string>();
        string[] rows = data.text.Trim().Split('\n');
        for (int i = 1; i < rows.Length; i++)
        {
            string[] colums = rows[i].Split(',');
            string heroName = colums[(int)column.Hero];
            heroNames.Add(heroName);
        }
        return heroNames;
    }
    /// <summary>
    /// Get IAttack by Attack Type Name
    /// </summary>
    /// <param name="IAttackName">Attack Type Name</param>
    /// <returns>Attack Type</returns>
    public static AttackBase FindIAttackByName(string IAttackName)
    {
        Type type = Type.GetType(IAttackName);
        ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);
        AttackBase attack = constructor.Invoke(new object[] { }) as AttackBase;
        return attack;
    }
    /// <summary>
    /// Get IEffect by Effect Type Name
    /// </summary>
    /// <param name="IEffect">Effect Type Name</param>
    /// <returns>Effect Type</returns>
    public static EffectBase FindIEffectByName(string IEffectName)
    {
        Type type = Type.GetType(IEffectName);
        ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);
        EffectBase attack = constructor.Invoke(new object[] { }) as EffectBase;
        return attack;
    }
    private enum column
    {
        Hero,
        AttackType,
        NumberOfTarget,
        Effect,
        EffectDuration

    }
}
