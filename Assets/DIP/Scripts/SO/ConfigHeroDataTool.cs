﻿using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ConfigHeroDataTool : OdinEditorWindow
{
    private TextAsset heroTypeData;
    [SerializeField]
    [ShowInInspector]
    private Dictionary<string, AttackBase> heroTypeDict = new Dictionary<string, AttackBase>();

    [MenuItem("Zitga/Config Hero Type")]
    private static void OpenWindow()
    {
        GetWindow<ConfigHeroDataTool>().Show();
    }
    private void OnEnable()
    {
        this.minSize = new Vector2(500, 500);
        heroTypeData = (TextAsset)AssetDatabase.LoadAssetAtPath("Assets/DIP/Scripts/SO/HeroTypeData.txt", typeof(TextAsset));
        heroTypeDict = DataConverter.GetDataFromHeroCSV(heroTypeData);
    }
    private string GetIAttackName(IAttack attack)
    {
        foreach (KeyValuePair<string, AttackBase> item in heroTypeDict)
        {
            if (item.Value.GetType().Name == attack.GetType().Name) return item.Value.GetType().Name;
        }
        return "khong co type nay!";
    }
    private string GetIEffectName(IEffect effect)
    {
        foreach (KeyValuePair<string, AttackBase> item in heroTypeDict)
        {
            if (item.Value.effect.GetType().Name == effect.GetType().Name) return item.Value.effect.GetType().Name;
        }
        return "khong co type nay!";
    }
    [Button]
    private void SaveChange()
    {
        string columName = heroTypeData.text.Split('\n')[0];
        File.WriteAllText("Assets/DIP/Scripts/SO/HeroTypeData.txt", String.Empty);
        using (StreamWriter writer = new StreamWriter(new FileStream("Assets/DIP/Scripts/SO/HeroTypeData.txt", FileMode.OpenOrCreate, FileAccess.Write)))
        {
            string line = columName;
            foreach (KeyValuePair<string, AttackBase> item in heroTypeDict)
            {
                line += $"\n{(item.Key)},{GetIAttackName(item.Value)},{item.Value.NumberOfTarget},{GetIEffectName(item.Value.effect)},{item.Value.effect.EffectDuration}";
            }
            writer.WriteLine(line);
            writer.Close();
            AssetDatabase.Refresh();
        }
    }
}
