﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Data Hero")]
public class DataHero : ScriptableObject
{
    [SerializeField]
    private TextAsset heroData;
    [ShowInInspector]
    [ReadOnly]
    public Dictionary<string, AttackBase> heroAttackTypes = new Dictionary<string, AttackBase>();
    private void OnEnable()
    {
        heroAttackTypes = DataConverter.GetDataFromHeroCSV(heroData);
    }
}
