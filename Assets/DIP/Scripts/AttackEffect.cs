﻿[System.Serializable]
public struct AttackEffect
{
    public IAttack attack;
    public IEffect effect;
}
