﻿using Composite;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    UpdateContainer updateSystemCompositeContainer;
    private void Awake()
    {
        updateSystemCompositeContainer = new UpdateContainer();
    }
    private void Update()
    {
        updateSystemCompositeContainer.UpdateByComposite();
    }
}
