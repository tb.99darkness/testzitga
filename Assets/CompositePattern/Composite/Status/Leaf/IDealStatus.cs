﻿namespace Composite
{
    public interface IDealStatus
    {
        void DealStatus(Hooman target);
    }
}
