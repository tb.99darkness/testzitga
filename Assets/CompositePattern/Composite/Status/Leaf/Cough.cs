﻿namespace Composite
{
    [System.Serializable]
    public class Cough : Status
    {
        public int duration;
        public float level;
        public override void DealStatus(Hooman target)
        {
            if (target.positivePercents <= 30)
            {
                if (target.resistanceLevel >= 5 && target.numberOfVaccineInjected == 2)
                {
                    return;
                }
            }
            target.Cough(duration, level);
        }
    }
}
