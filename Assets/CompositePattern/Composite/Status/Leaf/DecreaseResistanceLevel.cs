﻿namespace Composite
{
    public class DecreaseResistanceLevel : Status
    {
        public float decreaseLevel;
        public override void DealStatus(Hooman target)
        {
            target.DecreaseResistanceLevel((int)decreaseLevel * (1f - 0.45f * target.numberOfVaccineInjected));
        }
    }
}
