﻿namespace Composite
{
    [System.Serializable]
    public class Fever : Status
    {
        public int duration;
        public float temperature;
        public override void DealStatus(Hooman target)
        {
            if (target.positivePercents <= 30)
            {
                if (target.resistanceLevel >= 5 && target.numberOfVaccineInjected == 2)
                {
                    return;
                }
            }
            target.Fever(duration, temperature);
        }
    }
}
