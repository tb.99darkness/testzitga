﻿namespace Composite
{
    public class StatusContainer : Container<IDealStatus>, IDealStatus
    {
        public void DealStatus(Hooman target)
        {
            for (int i = 0; i < ts.Count; i++)
            {
                ts[i].DealStatus(target);
            }
        }
    }
}
