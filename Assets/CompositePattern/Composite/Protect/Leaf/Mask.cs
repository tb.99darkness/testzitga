﻿namespace Composite
{
    public class Mask : ProtectThing
    {
        public override void Protect(Hooman hooman)
        {
            hooman.WearMask();
        }
    }
}
