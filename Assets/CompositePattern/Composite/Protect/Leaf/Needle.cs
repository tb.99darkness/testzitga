﻿namespace Composite
{
    public class Needle : ProtectThing
    {
        public int increaseLevel;
        public override void Protect(Hooman hooman)
        {
            if (hooman.isCouch)
            {
                hooman.UnCough();
            }
            if (hooman.isFever)
            {
                hooman.UnFever();
            }
            hooman.Nagative();
            hooman.InjectVaccine(increaseLevel);
        }
    }
}
