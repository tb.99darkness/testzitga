﻿namespace Composite
{
    public interface IProtect
    {
        void Protect(Hooman hooman);
    }
}
