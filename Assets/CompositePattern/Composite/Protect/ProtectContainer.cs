﻿namespace Composite
{
    public class ProtectContainer : Container<IProtect>, IProtect
    {
        public void Protect(Hooman hooman)
        {
            for (int i = 0; i < ts.Count; i++)
            {
                ts[i].Protect(hooman);
            }
        }
    }
}
