﻿using System.Collections.Generic;

namespace Composite
{
    public class UpdateContainer : IUpdate

    {
        private static List<IUpdate> leafs;

        public UpdateContainer()
        {
            leafs = new List<IUpdate>();
        }

        public static void AddLeafs(IUpdate leaf)
        {
            leafs.Add(leaf);
        }

        public void UpdateByComposite()
        {
            foreach (var item in leafs)
            {
                item.UpdateByComposite();
            }
        }
    }

}
