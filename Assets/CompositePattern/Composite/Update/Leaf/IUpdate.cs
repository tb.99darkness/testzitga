﻿namespace Composite
{
    public interface IUpdate
    {
        void UpdateByComposite();
    }
}
