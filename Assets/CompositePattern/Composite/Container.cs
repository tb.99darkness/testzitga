﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Composite
{
    public class Container<T> : MonoBehaviour
    {
        protected List<T> ts;
        private void Awake()
        {
            GetLeaf();
        }
        protected virtual void GetLeaf()
        {
            ts = new List<T>();
            var leafs = GetComponentsInChildren<MonoBehaviour>().OfType<T>();
            foreach (T leaf in leafs)
            {
                ts.Add(leaf);
            }
            ts.RemoveAt(0);
        }
    }
}
