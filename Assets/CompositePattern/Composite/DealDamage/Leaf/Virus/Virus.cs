﻿using UnityEngine;
namespace Composite
{
    public class Virus : MonoBehaviour, IDealStatus, IDealDamage, ICondition
    {
        [SerializeField]
        protected float basePercent;
        [SerializeField]
        private StatusContainer statusContainer;
        [SerializeField]
        private ConditionContainer conditionContainer;

        public float ConditionBonus(Hooman hooman)
        {
            return conditionContainer.ConditionBonus(hooman);
        }

        public virtual void DealStatus(Hooman target)
        {
            statusContainer.DealStatus(target);
        }

        public void DealDamge(Hooman target)
        {
            float totalDamage = basePercent + ConditionBonus(target);
            if (target.isMaskOn)
            {
                totalDamage *= 0.5f;
            }
            target.GetDamage(totalDamage);
            DealStatus(target);
        }
    }
}
