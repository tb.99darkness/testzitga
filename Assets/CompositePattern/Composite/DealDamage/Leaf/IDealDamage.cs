﻿namespace Composite
{
    public interface IDealDamage
    {
        void DealDamge(Hooman target);
    }
}
