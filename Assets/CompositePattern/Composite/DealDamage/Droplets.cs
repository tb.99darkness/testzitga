﻿using UnityEngine;
namespace Composite
{
    public class Droplets : MonoBehaviour, IDealDamage
    {
        [SerializeField]
        private DamageContainer damageContainer;

        public void DealDamge(Hooman target)
        {
            damageContainer.DealDamge(target);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.transform.CompareTag(Tag.Hooman))
            {
                Hooman hooman = collision.transform.GetComponent<Hooman>();
                if (!hooman)
                {
                    Debug.Log("thieu component");
                    return;
                }
                DealDamge(hooman);
            }
        }
    }
}
