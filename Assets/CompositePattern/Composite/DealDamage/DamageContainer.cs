﻿namespace Composite
{
    public class DamageContainer : Container<IDealDamage>, IDealDamage
    {
        public virtual void DealDamge(Hooman target)
        {
            for (int i = 0; i < ts.Count; i++)
            {
                ts[i].DealDamge(target);
            }
        }
    }
}
