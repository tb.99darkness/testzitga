﻿namespace Composite
{
    public class ConditionContainer : Container<ICondition>, ICondition
    {
        public float ConditionBonus(Hooman hooman)
        {
            float bonus = 0;
            for (int i = 0; i < ts.Count; i++)
            {
                bonus += ts[i].ConditionBonus(hooman);
            }
            return bonus;
        }
    }
}
