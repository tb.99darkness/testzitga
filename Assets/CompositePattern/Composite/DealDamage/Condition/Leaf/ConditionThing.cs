﻿using UnityEngine;
namespace Composite
{
    public class ConditionThing : MonoBehaviour, ICondition
    {
        public virtual float ConditionBonus(Hooman hooman)
        {
            if (hooman.resistanceLevel <= 5)
            {
                return 20;
            }
            return 0;
        }

        public void DealDamge(Hooman target)
        {
        }
    }
}
