﻿using System.Collections.Generic;
using UnityEngine;

namespace Composite
{
    enum WeatherEnum
    {
        Wet,
        Dry
    }
    [System.Serializable]
    class WeatherInteractValue
    {
        public WeatherEnum weather;
        public float value;
    }
    public class Weather : ConditionThing
    {
        private WeatherEnum weather;
        [SerializeField]
        private List<WeatherInteractValue> weatherInteracts;
        public override float ConditionBonus(Hooman hooman)
        {
            float bonus = 0;
            if (weather.Equals(WeatherEnum.Wet))
            {
                bonus += weatherInteracts.Find(x => x.weather.Equals(weather)).value;
            }
            else if (weather.Equals(WeatherEnum.Dry))
            {
                bonus += weatherInteracts.Find(x => x.weather.Equals(weather)).value;
            }
            return bonus + base.ConditionBonus(hooman);
        }
    }
}
