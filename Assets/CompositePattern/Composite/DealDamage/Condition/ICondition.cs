﻿namespace Composite
{
    public interface ICondition
    {
        float ConditionBonus(Hooman hooman);
    }
}
