﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Composite
{
    public class Hooman : MonoBehaviour
    {
        public float positivePercents;
        public float resistanceLevel;
        public bool isFever;
        public bool isCouch;
        public float numberOfVaccineInjected;
        public bool isMaskOn;
        public List<Sprite> statusIcon;
        [SerializeField]
        private SpriteRenderer avatar;
        [SerializeField]
        private SpriteRenderer mask;
        [SerializeField]
        private UnityEvent<string> updateMessage;
        public void GetDamage(float percent)
        {
            percent *= (1 - (0.45f * numberOfVaccineInjected));
            positivePercents += percent;
            UpdateMessage($"{positivePercents}% nhiễm bệnh :'(");
            if (positivePercents >= 100)
            {
                UpdateMessage("Cách lý +1");
            }
        }
        public void Nagative()
        {
            positivePercents = 0;
            UpdateMessage($"{positivePercents}% nhiễm bệnh <3");
        }
        public void Fever(float duration, float temperature)
        {
            isFever = true;
            UpdateMessage($"Sốt {temperature} độ trong {duration} ngày");
            UpdateIcon();
        }
        public void Cough(float duration, float level)
        {
            isCouch = true;
            UpdateMessage($"Ho cấp độ {level} trong {duration} ngày! Hụ Hụ Hụ!");
            UpdateIcon();
        }
        public void UnFever()
        {
            isFever = false;
            UpdateMessage($"đã hết sốt");
            UpdateIcon();
        }
        public void UnCough()
        {
            isCouch = false;
            UpdateMessage($"đã khỏi ho");
            UpdateIcon();
        }
        public void DecreaseResistanceLevel(float value)
        {
            resistanceLevel -= value;
            resistanceLevel = resistanceLevel <= 0 ? 0 : resistanceLevel;
            UpdateMessage($"Sức đề kháng còn {resistanceLevel}!");
        }
        public void IncreaseResistanceLevel(int value)
        {
            resistanceLevel += value;
            resistanceLevel = resistanceLevel >= 10 ? 10 : resistanceLevel;
            UpdateMessage($"Sức đề kháng lên {resistanceLevel}!");
        }
        private void UpdateIcon()
        {
            avatar.sprite = statusIcon[0];
            if (isFever)
            {
                avatar.sprite = statusIcon[1];
            }
            if (isCouch)
            {
                avatar.sprite = statusIcon[2];
            }
            if (isFever && isCouch)
            {
                avatar.sprite = statusIcon[3];
            }
        }

        public void InjectVaccine(int increaseLevel)
        {
            IncreaseResistanceLevel(increaseLevel);
            if (numberOfVaccineInjected == 2)
            {
                UpdateMessage("đã đủ hai mũi!");
                return;
            }
            numberOfVaccineInjected++;
        }
        public void WearMask()
        {
            isMaskOn = true;
            mask.gameObject.SetActive(true);
        }

        private void UpdateMessage(string message)
        {
            updateMessage?.Invoke(message);
        }
    }
}
