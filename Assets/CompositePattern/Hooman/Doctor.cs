﻿using UnityEngine;
namespace Composite
{
    public class Doctor : Hooman, IProtect
    {
        [SerializeField]
        private ProtectContainer protectContainer;
        public void Protect(Hooman hooman)
        {
            protectContainer.Protect(hooman);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.transform.CompareTag(Tag.Hooman))
            {
                Hooman hooman = collision.transform.GetComponent<Hooman>();
                if (!hooman)
                {
                    Debug.Log("thieu component");
                    return;
                }
                Protect(hooman);
            }
        }
    }
}
