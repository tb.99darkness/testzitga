﻿using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI message;

    public void UpdateMessage(string messsage)
    {
        message.text += "\n" + "-" + messsage;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ClearMessage();
        }
    }
    [Button]
    public void ClearMessage()
    {
        message.text = "";
    }
}
