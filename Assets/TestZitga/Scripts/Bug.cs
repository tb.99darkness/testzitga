﻿using System.Collections.Generic;
using UnityEngine;

public class Bug : MonoBehaviour
{
    public List<Cell> rightPath = new List<Cell>();
    [SerializeField]
    private float speed = 0;
    // Update is called once per frame
    void Update()
    {
        if (rightPath.Count > 0)
        {
            Vector3 targetPos = rightPath[0].cellObject.transform.position;
            transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
            Vector3 direction = targetPos - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.AngleAxis(angle + 90, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 20);
            if (transform.position == rightPath[0].cellObject.transform.position)
            {
                rightPath.RemoveAt(0);
            }
        }
    }
}
