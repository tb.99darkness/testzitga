﻿using UnityEngine;

public class Cell
{
    public Vector2 gridPos;
    public GameObject cellObject;
    public CellScript cScript;
    public Cell()
    {

    }
    public Cell(Vector2 gridPos, GameObject cellObject, CellScript cScript)
    {
        this.gridPos = gridPos;
        this.cellObject = cellObject;
        this.cScript = cScript;
    }
    public Cell Clone()
    {
        return new Cell(this.gridPos, this.cellObject, this.cScript);
    }
}
