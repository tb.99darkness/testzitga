﻿using System.Collections.Generic;
using UnityEngine;

public class MazeGenerator : MonoBehaviour
{
    public int mazeRows;
    public int mazeColumns;
    public bool disableCellSprite;
    [SerializeField]
    private GameObject cellPrefab;
    // Dictionary to hold and locate all cells in maze.
    private Dictionary<Vector2, Cell> allCells = new Dictionary<Vector2, Cell>();
    // List to hold unvisited cells.
    private List<Cell> unvisited = new List<Cell>();
    // List to store 'stack' cells, cells being checked during generation.
    private List<Cell> stack = new List<Cell>();
    // Array will hold 4 centre room cells, from 0 -> 3 these are:
    // Top left (0), top right (1), bottom left (2), bottom right (3).
    private Cell[] centreCells = new Cell[4];
    // Cell variables to hold current and checking Cells.
    private Cell currentCell;
    private Cell checkCell;
    // Array of all possible neighbour positions.
    private Vector2[] neighbourPositions = new Vector2[] { new Vector2(-1, 0), new Vector2(1, 0), new Vector2(0, 1), new Vector2(0, -1) };
    // Size of the cells, used to determine how far apart to place cells during generation.
    private float cellSize;
    private float collumSize;
    private Cell startCell;
    private Vector2 endPos;

    private GameObject mazeParent;
    [SerializeField]
    private Bug bugPrefabs;
    [SerializeField]
    private Transform canvas;
    [SerializeField]
    private LineRenderer path;
    [SerializeField]
    private GameObject endPoint;
    private Bug bug;
    List<Cell> rightPath = new List<Cell>();

    private void Start()
    {
        GenerateMaze(mazeRows, mazeColumns);
    }

    private void GenerateMaze(int rows, int columns)
    {
        if (mazeParent != null) DeleteMaze();

        mazeRows = rows;
        mazeColumns = columns;
        CreateLayout();
    }

    // Creates the grid of cells.
    public void CreateLayout()
    {
        InitValues();

        // Set starting point, set spawn point to start.
        Vector2 startPos = new Vector2(-(cellSize * (mazeColumns / 2)) + (cellSize / 2), -(cellSize * (mazeRows / 2)) + (cellSize / 2));
        Vector2 spawnPos = startPos;

        for (int x = 1; x <= mazeColumns; x++)
        {
            for (int y = 1; y <= mazeRows; y++)
            {
                GenerateCell(spawnPos, new Vector2(x, y));
                // Increase spawnPos y.
                spawnPos.y += cellSize;
            }

            // Reset spawnPos y and increase spawnPos x.
            spawnPos.y = startPos.y;
            spawnPos.x += cellSize;
        }

        CreateCentre();
        RunAlgorithm();
        OpenWall();
        CheckWall();
        MakeExit();
        //FindPath();
    }

    private void OpenWall()
    {
        Cell cell = allCells[new Vector2((mazeColumns / 2), (mazeRows / 2) + 1)];
        //cell.cScript.Tick.SetActive(true);
        if (cell.cScript.wallL.activeSelf)
        {
            cell.cScript.wallL.SetActive(false);
            if (allCells.ContainsKey(cell.gridPos + neighbourPositions[0]))
            {
                allCells[cell.gridPos + neighbourPositions[0]].cScript.wallR.SetActive(false);
            }
            return;
        }
        if (cell.cScript.wallR.activeSelf)
        {
            cell.cScript.wallR.SetActive(false);
            if (allCells.ContainsKey(cell.gridPos + neighbourPositions[1]))
            {
                allCells[cell.gridPos + neighbourPositions[1]].cScript.wallR.SetActive(false);
            }
            return;
        }
        if (cell.cScript.wallU.activeSelf)
        {
            cell.cScript.wallU.SetActive(false);
            if (allCells.ContainsKey(cell.gridPos + neighbourPositions[2]))
            {
                allCells[cell.gridPos + neighbourPositions[2]].cScript.wallU.SetActive(false);
            }
            return;
        }
        if (cell.cScript.wallD.activeSelf)
        {
            cell.cScript.wallD.SetActive(false);
            if (allCells.ContainsKey(cell.gridPos + neighbourPositions[3]))
            {
                allCells[cell.gridPos + neighbourPositions[3]].cScript.wallD.SetActive(false);
            }
            return;
        }
        RemoveWall(centreCells[0].cScript, 4);
    }
    // This is where the fun stuff happens.
    public void RunAlgorithm()
    {
        // Get start cell, make it visited (i.e. remove from unvisited list).
        unvisited.Remove(currentCell);

        // While we have unvisited cells.
        while (unvisited.Count > 0)
        {
            List<Cell> unvisitedNeighbours = GetUnvisitedNeighbours(currentCell);
            if (unvisitedNeighbours.Count > 0)
            {
                // Get a random unvisited neighbour.
                checkCell = unvisitedNeighbours[Random.Range(0, unvisitedNeighbours.Count)];
                // Add current cell to stack.
                stack.Add(currentCell);
                // Compare and remove walls.
                CompareWalls(currentCell, checkCell);
                // Make currentCell the neighbour cell.
                currentCell = checkCell;
                // Mark new current cell as visited.
                unvisited.Remove(currentCell);
            }
            else if (stack.Count > 0)
            {
                // Make current cell the most recently added Cell from the stack.
                currentCell = stack[stack.Count - 1];
                // Remove it from stack.
                stack.Remove(currentCell);
            }
        }
    }

    public void MakeExit()
    {
        // Create and populate list of all possible edge cells.
        List<Cell> edgeCells = new List<Cell>();
        endPos = new Vector2(Random.Range(1, mazeColumns), Random.Range(1, mazeRows));
        Instantiate(endPoint, allCells[endPos].cellObject.transform.position, Quaternion.identity);
        foreach (KeyValuePair<Vector2, Cell> cell in allCells)
        {
            if (cell.Key.x == 0 || cell.Key.x == mazeColumns || cell.Key.y == 0 || cell.Key.y == mazeRows)
            {
                edgeCells.Add(cell.Value);
            }
        }
        // Get edge cell randomly from list.
        startCell = edgeCells[0];
        bug = Instantiate(bugPrefabs, startCell.cellObject.transform.position, Quaternion.identity);
    }

    public List<Cell> GetUnvisitedNeighbours(Cell curCell)
    {
        // Create a list to return.
        List<Cell> neighbours = new List<Cell>();
        // Create a Cell object.
        Cell nCell = curCell;
        // Store current cell grid pos.
        Vector2 cPos = curCell.gridPos;

        foreach (Vector2 p in neighbourPositions)
        {
            // Find position of neighbour on grid, relative to current.
            Vector2 nPos = cPos + p;
            // If cell exists.
            if (allCells.ContainsKey(nPos)) nCell = allCells[nPos];
            // If cell is unvisited.
            if (unvisited.Contains(nCell)) neighbours.Add(nCell);
        }

        return neighbours;
    }

    // Compare neighbour with current and remove appropriate walls.
    public void CompareWalls(Cell cCell, Cell nCell)
    {
        // If neighbour is left of current.
        if (nCell.gridPos.x < cCell.gridPos.x)
        {
            RemoveWall(nCell.cScript, 2);
            RemoveWall(cCell.cScript, 1);
        }
        // Else if neighbour is right of current.
        else if (nCell.gridPos.x > cCell.gridPos.x)
        {
            RemoveWall(nCell.cScript, 1);
            RemoveWall(cCell.cScript, 2);
        }
        // Else if neighbour is above current.
        else if (nCell.gridPos.y > cCell.gridPos.y)
        {
            RemoveWall(nCell.cScript, 4);
            RemoveWall(cCell.cScript, 3);
        }
        // Else if neighbour is below current.
        else if (nCell.gridPos.y < cCell.gridPos.y)
        {
            RemoveWall(nCell.cScript, 3);
            RemoveWall(cCell.cScript, 4);
        }
    }

    // Function disables wall of your choosing, pass it the script attached to the desired cell
    // and an 'ID', where the ID = the wall. 1 = left, 2 = right, 3 = up, 4 = down.
    public void RemoveWall(CellScript cScript, int wallID)
    {
        if (wallID == 1) cScript.wallL.SetActive(false);
        else if (wallID == 2) cScript.wallR.SetActive(false);
        else if (wallID == 3) cScript.wallU.SetActive(false);
        else if (wallID == 4) cScript.wallD.SetActive(false);
    }

    public void CreateCentre()
    {
        // Get the 4 centre cells using the rows and columns variables.
        // Remove the required walls for each.
        centreCells[0] = allCells[new Vector2((mazeColumns / 2), (mazeRows / 2) + 1)];
        RemoveWall(centreCells[0].cScript, 4);
        RemoveWall(centreCells[0].cScript, 2);
        // Create a List of ints, using this, select one at random and remove it.
        // We then use the remaining 3 ints to remove 3 of the centre cells from the 'unvisited' list.
        // This ensures that one of the centre cells will connect to the maze but the other three won't.
        // This way, the centre room will only have 1 entry / exit point.
        List<int> rndList = new List<int> { 0 };
        int startCell = rndList[Random.Range(0, rndList.Count)];
        rndList.Remove(startCell);
        currentCell = centreCells[startCell];
        foreach (int c in rndList)
        {
            unvisited.Remove(centreCells[c]);
        }
    }

    public void GenerateCell(Vector2 pos, Vector2 keyPos)
    {
        // Create new Cell object.
        Cell newCell = new Cell();

        // Store reference to position in grid.
        newCell.gridPos = keyPos;
        // Set and instantiate cell GameObject.
        newCell.cellObject = Instantiate(cellPrefab, pos, cellPrefab.transform.rotation);
        // Child new cell to parent.
        if (mazeParent != null) newCell.cellObject.transform.parent = mazeParent.transform;
        // Set name of cellObject.
        newCell.cellObject.name = "Cell - X:" + keyPos.x + " Y:" + keyPos.y;
        // Get reference to attached CellScript.
        newCell.cScript = newCell.cellObject.GetComponent<CellScript>();
        // Disable Cell sprite, if applicable.
        if (disableCellSprite) newCell.cellObject.GetComponent<SpriteRenderer>().enabled = false;

        // Add to Lists.
        allCells[keyPos] = newCell;
        unvisited.Add(newCell);
    }

    public void DeleteMaze()
    {
        if (mazeParent != null) Destroy(mazeParent);
    }

    public void InitValues()
    {
        // Check generation values to prevent generation failing.
        if (IsOdd(mazeRows)) mazeRows--;
        if (IsOdd(mazeColumns)) mazeColumns--;

        if (mazeRows <= 3) mazeRows = 4;
        if (mazeColumns <= 3) mazeColumns = 4;

        // Determine size of cell using localScale.
        cellSize = cellPrefab.transform.localScale.x / 2;
        collumSize = cellPrefab.transform.localScale.y / 2;

        // Create an empty parent object to hold the maze in the scene.
        mazeParent = new GameObject();
        mazeParent.transform.position = Vector2.zero;
        mazeParent.name = "Maze";
    }

    public bool IsOdd(int value)
    {
        return value % 2 != 0;
    }
    // check wall
    private void CheckWall()
    {
        foreach (KeyValuePair<Vector2, Cell> item in allCells)
        {

            Cell cell = item.Value;
            if (cell.cScript.wallL.activeSelf)
            {
                if (allCells.ContainsKey(cell.gridPos + neighbourPositions[0]))
                {
                    allCells[cell.gridPos + neighbourPositions[0]].cScript.wallR.SetActive(true);
                }
            }
            if (cell.cScript.wallR.activeSelf)
            {
                if (allCells.ContainsKey(cell.gridPos + neighbourPositions[1]))
                {
                    allCells[cell.gridPos + neighbourPositions[1]].cScript.wallL.SetActive(true);
                }
            }

            if (cell.cScript.wallU.activeSelf)
            {
                if (allCells.ContainsKey(cell.gridPos + neighbourPositions[2]))
                {
                    allCells[cell.gridPos + neighbourPositions[2]].cScript.wallD.SetActive(true);
                }
            }
            if (cell.cScript.wallD.activeSelf)
            {
                if (allCells.ContainsKey(cell.gridPos + neighbourPositions[3]))
                {
                    allCells[cell.gridPos + neighbourPositions[3]].cScript.wallU.SetActive(true);
                }
            }
        }

    }

    // Find the right path
    public void FindNearestPath()
    {
        Vector2 bugPos = startCell.gridPos;
        List<DirectCell> avaiableDirCells = new List<DirectCell>();
        List<Cell> visitedCell = new List<Cell>();
        rightPath = new List<Cell>();
        for (int i = 0; i < mazeColumns * mazeRows * 2; i++)
        {
            // Tick the path was visited
            visitedCell.Add(allCells[bugPos]);
            rightPath.Add(allCells[bugPos]);
            // Detect avaiable way
            List<Cell> directCell = GetDirectionOfCeil(bugPos);
            // check and save avaiable path
            if (directCell.Count >= 2)
            {
                for (int j = directCell.Count - 1; j >= 0; j--)
                {
                    if (visitedCell.Contains(directCell[j]))
                    {
                        directCell.RemoveAt(j);
                    }
                }
                if (directCell.Count > 0)
                {
                    // chose random way to move
                    int randomDir = Random.Range(0, directCell.Count - 1);
                    bugPos = directCell[randomDir].gridPos;
                    rightPath.Add(allCells[bugPos]);
                    if (bugPos == endPos)
                    {
                        Debug.Log("da tim thay duong di");
                        DrawLine(rightPath);
                        return;
                    }
                    directCell.RemoveAt(randomDir);
                    if (directCell.Count > 0)
                    {
                        avaiableDirCells.Add(new DirectCell(allCells[bugPos], directCell));
                    }
                }
            }
            else
            {
                // trường hợp đặc biết khi điểm bắt đầu chỉ có 1 đường đi
                if (i == 0)
                {
                    for (int j = directCell.Count - 1; j >= 0; j--)
                    {
                        if (visitedCell.Contains(directCell[j]))
                        {
                            directCell.RemoveAt(j);
                        }
                    }
                    // chose random way to move
                    int randomDir = Random.Range(0, directCell.Count - 1);
                    bugPos = directCell[randomDir].gridPos;
                    rightPath.Add(allCells[bugPos]);
                    if (bugPos == endPos)
                    {
                        Debug.Log("da tim thay duong di");
                        DrawLine(rightPath);
                        return;
                    }
                    directCell.RemoveAt(randomDir);
                    if (directCell.Count > 0)
                    {
                        avaiableDirCells.Add(new DirectCell(allCells[bugPos], directCell));
                    }
                }
                // back to the last node
                if (avaiableDirCells.Count > 0)
                {
                    List<Cell> cells = avaiableDirCells[avaiableDirCells.Count - 1].DirectCells;
                    bugPos = cells[cells.Count - 1].gridPos;
                    rightPath.Add(allCells[bugPos]);
                    // remove wrong way
                    for (int j = 0; j < rightPath.Count; j++)
                    {
                        if (rightPath[j].Equals(avaiableDirCells[avaiableDirCells.Count - 1].MainCell))
                        {
                            for (int g = rightPath.Count - 1; g >= j; g--)
                            {
                                rightPath.RemoveAt(g);
                            }
                        }
                    }
                    if (bugPos == endPos)
                    {
                        Debug.Log("da tim thay duong di");
                        DrawLine(rightPath);
                        return;
                    }
                    cells.RemoveAt(cells.Count - 1);
                    if (cells.Count < 1)
                    {
                        avaiableDirCells.RemoveAt(avaiableDirCells.Count - 1);
                    }
                }
            }
        }
    }

    public void FindfurthestPath()
    {
        Vector2 bugPos = startCell.gridPos;
        List<DirectCell> avaiableDirCells = new List<DirectCell>();
        List<Cell> visitedCell = new List<Cell>();
        List<List<Cell>> allRightPath = new List<List<Cell>>();
        List<Cell> rightPathTemp = new List<Cell>();
        for (int i = 0; i < mazeColumns * mazeRows * 2; i++)
        {
            // Tick the path was visited
            visitedCell.Add(allCells[bugPos]);
            //rightPath.Add(allCells[bugPos]);
            // allCells[bugPos].cScript.Tick.SetActive(true);
            // Detect avaiable way
            List<Cell> directCell = GetDirectionOfCeil(bugPos);
            // check and save avaiable path
            for (int j = directCell.Count - 1; j >= 0; j--)
            {
                if (visitedCell.Contains(directCell[j]))
                {
                    directCell.RemoveAt(j);
                }
            }
            if (directCell.Count > 0)
            {
                // chose random way to move
                int randomDir = Random.Range(0, directCell.Count - 1);
                if (directCell.Count - 1 > 0)
                {
                    avaiableDirCells.Add(new DirectCell(allCells[bugPos], directCell));
                }
                bugPos = directCell[randomDir].gridPos;
                rightPathTemp.Add(allCells[bugPos]);
                if (bugPos == endPos)
                {
                    Debug.Log("da tim thay duong di");
                    allRightPath.Add(rightPathTemp.ConvertAll(x => x.Clone()));
                    //DrawLine(rightPathTemp);
                    if (directCell.Count - 2 > 0)
                    {
                        for (int j = 0; j < directCell.Count; j++)
                        {
                            if (j != randomDir)
                            {
                                bugPos = directCell[j].gridPos;
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (avaiableDirCells.Count < 2)
                        {
                            break;
                        }
                        randomDir = Random.Range(0, avaiableDirCells[avaiableDirCells.Count - 2].DirectCells.Count - 1);
                        bugPos = avaiableDirCells[avaiableDirCells.Count - 2].DirectCells[randomDir].gridPos;
                    }
                    avaiableDirCells.RemoveAt(avaiableDirCells.Count - 1);
                    ClearAPath(ref visitedCell, ref rightPathTemp, avaiableDirCells[avaiableDirCells.Count - 1].MainCell);
                    continue;
                }
            }
            else
            {
                // trường hợp đặc biết khi điểm bắt đầu chỉ có 1 đường đi
                if (i == 0)
                {
                    for (int j = directCell.Count - 1; j >= 0; j--)
                    {
                        if (visitedCell.Contains(directCell[j]))
                        {
                            directCell.RemoveAt(j);
                        }
                    }
                    // chose random way to move
                    int randomDir = Random.Range(0, directCell.Count - 1);
                    if (directCell.Count - 1 > 0)
                    {
                        avaiableDirCells.Add(new DirectCell(allCells[bugPos], directCell));
                    }
                    bugPos = directCell[randomDir].gridPos;
                    rightPathTemp.Add(allCells[bugPos]);
                    if (bugPos == endPos)
                    {
                        Debug.Log("da tim thay duong di");
                        allRightPath.Add(rightPathTemp.ConvertAll(x => x.Clone()));
                        //DrawLine(rightPathTemp);
                        ClearAPath(ref visitedCell, ref rightPathTemp, avaiableDirCells[avaiableDirCells.Count - 1].MainCell);
                        avaiableDirCells.RemoveAt(avaiableDirCells.Count - 1);
                        continue;
                    }
                }
                // back to the last node
                if (avaiableDirCells.Count > 0)
                {
                    for (int h = 0; h < avaiableDirCells.Count; h++)
                    {

                    }
                    List<Cell> cells = avaiableDirCells[avaiableDirCells.Count - 1].DirectCells;
                    // remove wrong way
                    for (int j = 0; j < rightPathTemp.Count; j++)
                    {
                        if (rightPathTemp[j].Equals(avaiableDirCells[avaiableDirCells.Count - 1].MainCell))
                        {
                            for (int g = rightPathTemp.Count - 1; g > j; g--)
                            {
                                rightPathTemp.RemoveAt(g);
                            }
                            break;
                        }
                    }

                    bugPos = cells[cells.Count - 1].gridPos;
                    rightPathTemp.Add(allCells[bugPos]);
                    if (bugPos == endPos)
                    {
                        Debug.Log("da tim thay duong di");
                        allRightPath.Add(rightPathTemp.ConvertAll(x => x.Clone()));
                        //DrawLine(rightPathTemp);
                        ClearAPath(ref visitedCell, ref rightPathTemp, avaiableDirCells[avaiableDirCells.Count - 1].MainCell);
                        avaiableDirCells.RemoveAt(avaiableDirCells.Count - 1);
                        continue;
                    }
                    cells.RemoveAt(cells.Count - 1);
                    if (cells.Count < 1)
                    {
                        avaiableDirCells.RemoveAt(avaiableDirCells.Count - 1);
                    }
                }
            }
        }
        for (int i = 0; i < allRightPath.Count; i++)
        {
            if (allRightPath[i].Count > rightPath.Count)
            {
                rightPath = allRightPath[i];
            }
        }
        DrawLine(rightPath);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="cells">current right path</param>
    /// <param name="cell">nearest node</param>
    private void ClearAPath(ref List<Cell> visited, ref List<Cell> cells, Cell cell)
    {
        for (int i = 0; i < cells.Count; i++)
        {
            if (cells[i].Equals(cell))
            {
                for (int g = cells.Count - 1; g >= i; g--)
                {
                    visited.Remove(cells[g]);
                    cells.RemoveAt(g);
                }
                return;
            }
        }
    }
    // draw line renderer
    private void DrawLine(List<Cell> cells)
    {
        LineRenderer line = Instantiate(path);
        List<Vector3> vP = new List<Vector3>();
        for (int i = 0; i < cells.Count; i++)
        {
            vP.Add(cells[i].cellObject.transform.position);
        }
        int randomR = Random.Range(100, 255);
        int randomG = Random.Range(100, 255);
        int randomB = Random.Range(100, 255);
        Gradient gradient = new Gradient();
        gradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(new Color(randomR, randomG, randomB), 0.0f), new GradientColorKey(new Color(randomR, randomG, randomB), 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(1f, 0.0f), new GradientAlphaKey(1f, 1.0f) }
        );
        line.colorGradient = gradient;
        line.positionCount = vP.Count;
        line.SetPositions(vP.ToArray());
    }

    public void MoveBug()
    {
        bug.rightPath = this.rightPath;
    }
    // find avaiable way
    private List<Cell> GetDirectionOfCeil(Vector2 cellPos)
    {
        List<Cell> dir = new List<Cell>();
        if (allCells.ContainsKey(cellPos))
        {
            Cell cell = allCells[cellPos];
            if (!cell.cScript.wallL.activeSelf)
            {
                if (allCells.ContainsKey(cellPos + neighbourPositions[0]))
                {
                    dir.Add(allCells[cellPos + neighbourPositions[0]]);
                }
            }
            if (!cell.cScript.wallR.activeSelf)
            {
                if (allCells.ContainsKey(cellPos + neighbourPositions[1]))
                {
                    dir.Add(allCells[cellPos + neighbourPositions[1]]);
                }
            }

            if (!cell.cScript.wallU.activeSelf)
            {
                if (allCells.ContainsKey(cellPos + neighbourPositions[2]))
                {
                    dir.Add(allCells[cellPos + neighbourPositions[2]]);
                }
            }
            if (!cell.cScript.wallD.activeSelf)
            {
                if (allCells.ContainsKey(cellPos + neighbourPositions[3]))
                {
                    dir.Add(allCells[cellPos + neighbourPositions[3]]);
                }
            }
        }
        return dir;
    }

    private class DirectCell
    {
        public Cell MainCell;
        public List<Cell> DirectCells = new List<Cell>();
        public DirectCell(Cell MainCell, List<Cell> DirectCells)
        {
            this.MainCell = MainCell;
            this.DirectCells = DirectCells;
        }
    }
}
