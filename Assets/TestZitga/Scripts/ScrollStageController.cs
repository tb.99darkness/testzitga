﻿using SuperScrollView;
using System.Collections.Generic;
using UnityEngine;


public class ScrollStageController : MonoBehaviour
{
    [SerializeField]
    private List<Stage> StagesInfo = new List<Stage>();
    [SerializeField]
    private LoopGridView LoopGridView;
    private bool isLoopViewInit;

    public void InitScrollView(List<Stage> stages)
    {
        LoopGridView.MovePanelToItemByIndex(200);
        if (!isLoopViewInit)
        {
            StagesInfo = stages;
            LoopGridView.InitGridView(StagesInfo.Count, OnGetItemByRowColumn);
            isLoopViewInit = true;
        }
        else
        {
            this.StagesInfo.Clear();
            StagesInfo = stages;
            LoopGridView.SetListItemCount(this.StagesInfo.Count, false);
        }
        //this.refesh.interactable = true;
    }

    private Stage GetDataByIndex(int index)
    {
        return this.StagesInfo[index];
    }


    LoopGridViewItem OnGetItemByRowColumn(LoopGridView gridView, int itemIndex, int row, int column)
    {
        if (itemIndex < 0 || itemIndex >= this.StagesInfo.Count)
        {
            return null;
        }
        LoopGridViewItem item = gridView.NewListViewItem("Stage");
        Stage data = this.GetDataByIndex(itemIndex);
        StageScrollItem itemScript = item.GetComponent<StageScrollItem>();

        if (item.IsInitHandlerCalled == false)
        {
            item.IsInitHandlerCalled = true;
        }
        itemScript.Stage = data;
        itemScript.DisplayInfo();
        return item;
    }
}
