﻿using UnityEngine;
using UnityEngine.UI;

public class Stars : MonoBehaviour
{
    [SerializeField]
    private Image[] images;

    public void ShowStar(int numberOfStars)
    {
        for (int i = 0; i < numberOfStars; i++)
        {
            images[i].enabled = true;
        }
        for (int i = numberOfStars; i < images.Length; i++)
        {
            images[i].enabled = false;
        }
    }
}
