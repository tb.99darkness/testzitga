﻿using UnityEngine;

public class Stage : MonoBehaviour
{
    public int StageNumber = 0;
    public bool IsUnLock = true;
    public int Star = 0;

    public Stage(int stageNumber, bool isLock, int star)
    {
        this.StageNumber = stageNumber;
        this.IsUnLock = isLock;
        this.Star = star;
    }
}
