﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StageScrollItem : MonoBehaviour
{
    [HideInInspector]
    public Stage Stage;
    [SerializeField]
    private TextMeshProUGUI text;
    [SerializeField]
    private Image lockImage;
    [SerializeField]
    private Button buttonStage;
    [SerializeField]
    private Image tutorialImage;
    [SerializeField]
    private Stars stars;
    public void DisplayInfo()
    {
        text.text = "";
        lockImage.enabled = !Stage.IsUnLock;
        buttonStage.interactable = Stage.IsUnLock;
        tutorialImage.enabled = Stage.StageNumber == 0;
        text.text = (Stage.StageNumber + 1).ToString();
        stars.ShowStar(Stage.Star);
    }

}
