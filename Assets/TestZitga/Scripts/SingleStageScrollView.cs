﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SingleStageScrollView : MonoBehaviour
{
    [HideInInspector]
    public Stage Stage;
    [SerializeField]
    private TextMeshProUGUI text;
    [SerializeField]
    private Image lockImage;
    [SerializeField]
    private Button buttonStage;
    [SerializeField]
    private Image tutorialImage;

    public void DisplayInfo()
    {
        text.text = "";
        lockImage.enabled = !Stage.IsUnLock;
        buttonStage.interactable = Stage.IsUnLock;
        if (Stage.StageNumber == 0)
        {
            tutorialImage.enabled = true;
            return;
        }
        text.text = (Stage.StageNumber + 1).ToString();
    }
}
