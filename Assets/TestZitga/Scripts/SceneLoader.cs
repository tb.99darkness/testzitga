﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private string Maze = "Maze";
    private string MainMenu = "MainMenu";
    public void LoadMaze()
    {
        SceneManager.LoadScene(Maze, LoadSceneMode.Single);
    }
    public void LoadMainMenu()
    {
        SceneManager.LoadScene(MainMenu, LoadSceneMode.Single);
    }
}
