﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private ScrollStageController scrollStageController;
    [SerializeField]
    private TextMeshProUGUI starCount;
    [SerializeField]
    private GameObject Scroll;
    // Start is called before the first frame update
    void Start()
    {
        GenarateStage();
    }

    public void GenarateStage()
    {
        int numbersOfStageUnlock = 0;
        if (PlayerPrefs.HasKey("numbersOfStageUnlock"))
        {
            numbersOfStageUnlock = PlayerPrefs.GetInt("numbersOfStageUnlock", 10);
        }
        else
        {
            numbersOfStageUnlock = Random.Range(0, 150);
            PlayerPrefs.SetInt("numbersOfStageUnlock", numbersOfStageUnlock);
        }
        List<Stage> stagesTemp = new List<Stage>();
        List<Stage> stages = new List<Stage>();
        int allstar = 0;
        for (int i = 0; i < numbersOfStageUnlock; i++)
        {

            int star = 0;
            if (PlayerPrefs.HasKey($"{i}"))
            {
                star = PlayerPrefs.GetInt($"{i}", 1);
            }
            else
            {
                star = Random.Range(1, 3);
                PlayerPrefs.SetInt($"{i}", star);
            }
            Stage stage = new Stage(i, true, star);
            stagesTemp.Add(stage);
            allstar += star;
            if ((i + 1) % 8 == 0)
            {
                stagesTemp.Reverse();
            }
            if ((i + 1) % 4 == 0)
            {
                stages.AddRange(stagesTemp);
                stagesTemp.Clear();
            }
        }
        starCount.text = allstar.ToString();
        for (int i = numbersOfStageUnlock; i < 999 - numbersOfStageUnlock; i++)
        {
            Stage stage = new Stage(i, false, 0);
            stagesTemp.Add(stage);
            if ((i + 1) % 8 == 0)
            {
                stagesTemp.Reverse();
            }
            if ((i + 1) % 4 == 0)
            {
                stages.AddRange(stagesTemp);
                stagesTemp.Clear();
            }
        }
        scrollStageController.InitScrollView(stages);
    }

    public void ResetStages()
    {
        List<Stage> stagesTemp = new List<Stage>();
        List<Stage> stages = new List<Stage>();
        Stage stage = new Stage(0, true, 0);
        stagesTemp.Add(stage);
        for (int i = 1; i < 998; i++)
        {
            stage = new Stage(i, false, 0);
            stagesTemp.Add(stage);
            if ((i + 1) % 8 == 0)
            {
                stagesTemp.Reverse();
            }
            if ((i + 1) % 4 == 0)
            {
                stages.AddRange(stagesTemp);
                stagesTemp.Clear();
            }
        }
        starCount.text = "0";
        scrollStageController.InitScrollView(stages);
    }
}
