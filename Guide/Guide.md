# Guide
## `Doi Attack Type cua 1 tuong`
- Buoc 1: `Ziga` -> `Config Hero Type`

<img src=".img/zitgatool1.png"/>

<img src=".img/zitgatool2.png"/>

- Buoc 2: click vao `Value` cua hero muon doi

<img src=".img/zitgatool3.png"/>

- Buoc 3: luu

<img src=".img/zitgatool4.png"/>

## `Test` 
-  mo scene `DIP`
  
  <img src=".img/DIPscene.png"/>

- `Start`

- mo cua so `Inspector` cua `Hero`

  <img src=".img/HeroInspector.png"/>
- Click `Attack` -> check log unity